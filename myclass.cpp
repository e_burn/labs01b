#include <iostream>
#pragma warning(disable : 4996)
#include <string>
#include<conio.h>
#include <map>
#include <fstream>
#include <cctype>
#include "myclass.h"
#include <utility>
#include <vector>
#include <regex>
using namespace std;
	void myclass::work(char* argv[])
	{
		ifstream input;
		ofstream output;
		input.open(argv[1]);
		output.open(argv[2]);
	 //___________________
		int sum = 0;
		regex r("[\\w]+");
		while ((getline(input, word)))
		{
			while (regex_search(word, s, r))
			{
				sum++;
				myFirstMap[s[0]]++;
				word = s.suffix().str();
			}
		}
		vector<pair<string, int>> reverse;
		for (auto cur = myFirstMap.rbegin(); cur != myFirstMap.rend(); cur++)
			reverse.push_back(make_pair((*cur).first, (*cur).second));

		sort(reverse.begin(), reverse.end(), [](auto &left, auto &right) {
			return left.second > right.second;
		});
		
			for (auto ix = reverse.begin(); ix != reverse.end(); ++ix) 
				output << ix->first << ';' << ix->second << ';' << (float)((float)ix->second / (float)sum) * 100 << "%\n";
	  //___________________
}
